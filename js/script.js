const nameBtnUI = $('#name');
const peopleBtnUI = $('#people');
const searchBtnUI = $('#search-btn');

const entriesPerPage = 20;
const apiKey = '7d0c51211215cdb90fc782f688e3a1d9';

let currentMethod = 'name';

const findMoviesByName = async (query, page = 1) => {
  return await fetch(
    `https://api.themoviedb.org/3/search/movie?api_key=${apiKey}&page=${page}&include_adult=false&query=${query}`
  ).then(res => res.json());
};

const findMoviesByPeople = async (query, page = 1) => {
  const people = await fetch(
    `https://api.themoviedb.org/3/search/person?api_key=${apiKey}&page=1&include_adult=false&query=${query}`
  ).then(res => res.json());
  if (people.total_results > 0) {
    const peopleId = people.results[0].id;
    const moviesCredit = await fetch(
      `https://api.themoviedb.org/3/person/${peopleId}/movie_credits?api_key=${apiKey}`
    ).then(res => res.json());
    if (moviesCredit.cast.length > 0) {
      const results = moviesCredit.cast.slice((page - 1) * entriesPerPage, entriesPerPage * page);
      return {
        page,
        total_results: moviesCredit.cast.length,
        total_pages: Math.ceil(moviesCredit.cast.length / entriesPerPage),
        results
      };
    }
  }
};

const fillMovies = movies => {
  let order = 1;
  let htmlContent = '';
  for (let i = 0; i < movies.results.length; i++) {
    if (order === 1) {
      htmlContent += '<div class="row justify-content-center card-deck text-left">';
    }
    let imgLink;
    if (!movies.results[i].poster_path) {
      imgLink = 'img/no-poster.png';
    } else {
      imgLink = `https://image.tmdb.org/t/p/w500/${movies.results[i].poster_path}`;
    }
    htmlContent += `
      <div class="col-sm card mb-3 mx-2 movie-card p-0" id="mov-${i}">
      <div class="row no-gutters">
        <div class="col-md-5">
          <img src="${imgLink}" class="card-img" alt="${movies.results[i].title} Poster" />
        </div>
        <div class="col-md-7">
          <div class="card-body p-3 container-fluid">
            <h5 class="card-title" data-toggle="tooltip" data-placement="top" title="${movies.results[i].title}">${
      movies.results[i].title
    }</h5>
            ${
              movies.results[i].vote_count > 0
                ? `<h6 class="card-subtitle mb-2 text-muted">Rating: ${movies.results[i].vote_average} (${movies.results[i].vote_count})</h6>`
                : ''
            }
            
            <div class="scrollable m-0 p-0">
              <p class="card-text">${movies.results[i].overview}</p>
            </div>
          </div>
        </div>
      </div>
      </div>`;
    if (order === 3) {
      htmlContent += '</div>';
      order = 0;
    }
    order++;
  }
  $('#result')
    .empty()
    .append(htmlContent);
  for (let i = 0; i < movies.results.length; i++) {
    $(`#mov-${i}`).click(() => {
      getMovieDetail(movies.results[i].id);
    });
  }
};

const createPagination = (query, method, movies) => {
  let htmlContent = `
    <div class="dropdown text-center">
      <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Page ${movies.page}/${movies.total_pages}
      </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">`;
  for (let i = 1; i <= movies.total_pages; i++) {
    htmlContent += `<a class="dropdown-item" id="page-${i}">${i}</a>`;
    $(`#page-${i}`).click(() => {
      searchMovie(query, currentMethod, i);
    });
  }
  htmlContent += `</div></div>`;
  $('#result').append(htmlContent);
  for (let i = 1; i <= movies.total_pages; i++) {
    $(`#page-${i}`).click(() => {
      searchMovie(query, currentMethod, i);
    });
  }
};

const getMovie = async id => {
  return await fetch(`https://api.themoviedb.org/3/movie/${id}?api_key=${apiKey}`).then(res => res.json());
};

const getGenreList = genreArray => {
  let result = '';
  genreArray.forEach(g => {
    result += `${g.name} / `;
  });
  return result.slice(0, -3);
};

const getDirector = crewArray => {
  return crewArray.find(c => c.job === 'Director');
};

const getActorList = castArray => {
  let result = '';
  for (let i = 0; i < 10; i++) {
    if (i >= castArray.length) break;
    result += `<span id="actor-${i}" class="actor-name">${castArray[i].name}</span> / `;
  }
  return result.slice(0, -3);
};

const fillMovieDetail = async movie => {
  let imgLink;
  if (!movie.poster_path) {
    imgLink = 'img/no-poster.png';
  } else {
    imgLink = `https://image.tmdb.org/t/p/w500/${movie.poster_path}`;
  }
  const htmlContent = `
    <div class="row text-left">
      <div class="col-2">
        <img class="img-thumbnail" src="${imgLink}"></img>
      </div>
        <div class="col-10">
          <h1>${movie.title}</h1>
          <p class="">${movie.overview}</p>
          <dl class="row">
            <dt class="col-sm-2">Release Date</dt>
            <dd class="col-sm-10">${movie.release_date}</dd>
            <dt class="col-sm-2">Genres</dt>
            <dd class="col-sm-10">${getGenreList(movie.genres)}</dd>
            <dt class="col-sm-2">Director</dt>
            <dd class="col-sm-10" id="director-name"></dd>
            <dt class="col-sm-2">Actors</dt>
            <dd class="col-sm-10" id="actors"></dd>
          </dl>
        </div>
    </div>`;
  $('#result')
    .empty()
    .append(htmlContent);
  const credit = await fetch(`https://api.themoviedb.org/3/movie/${movie.id}/credits?api_key=${apiKey}`).then(res =>
    res.json()
  );
  $('#director-name').append(getDirector(credit.crew).name);
  $('#actors').append(getActorList(credit.cast));
  for (let i = 0; i < 10; i++) {
    if (i >= credit.cast.length) break;
    $(`#actor-${i}`).click(() => {
      getPersonDetail(credit.cast[i].id);
    });
  }
  const reviews = await fetch(
    `https://api.themoviedb.org/3/movie/${movie.id}/reviews?api_key=${apiKey}&language=en-US`
  ).then(res => res.json());
  if (reviews.total_results > 0) {
    $('#result').append(getReviewList(reviews.results));
  }
};

const getReviewList = reviews => {
  let htmlContent = `<div class="container-fluid mt-4 text-left">
                      <h2>Reviews</h2>
                      <ul class="list-unstyled">`;
  reviews.forEach(review => {
    htmlContent += `
      <li class="media my-4">
        <div class="media-body">
          <h5 class="mt-0 mb-1">${review.author}</h5>
          ${review.content}
        </div>
      </li>`;
  });
  htmlContent += `</ul></div>`;
  return htmlContent;
};

const getPerson = async id => {
  return await fetch(`https://api.themoviedb.org/3/person/${id}?api_key=${apiKey}`).then(res => res.json());
};

const getCastList = movies => {
  let result = '';
  for (let i = 0; i < 10; i++) {
    if (i >= movies.length) break;
    result += `<span class="movie-title" id="mov-${i}">${movies[i].title} (${movies[i].character})</span> / `;
  }
  return result.slice(0, -3);
};

const fillPersonDetail = async person => {
  let imgLink;
  if (!person.profile_path) {
    imgLink = 'img/no-poster.png';
  } else {
    imgLink = `https://image.tmdb.org/t/p/w500/${person.profile_path}`;
  }
  const htmlContent = `
    <div class="row text-left">
      <div class="col-2">
        <img class="img-thumbnail" src="${imgLink}"></img>
      </div>
      <div class="col-10">
        <h1>${person.name}</h1>
        <p class="">${person.biography}</p>
        <dl class="row">
          <dt class="col-sm-1">Cast</dt>
          <dd class="col-sm-11" id ="cast-list"></dd>
        </dl>
      </div>
    </div>`;
  $('#result')
    .empty()
    .append(htmlContent);
  const movieCredit = await fetch(
    `https://api.themoviedb.org/3/person/${person.id}/movie_credits?api_key=${apiKey}`
  ).then(res => res.json());
  $('#cast-list').append(getCastList(movieCredit.cast));
  for (let i = 0; i < 10; i++) {
    if (i >= movieCredit.cast.length) break;
    $(`#mov-${i}`).click(() => {
      getMovieDetail(movieCredit.cast[i].id);
    });
  }
};

const searchMovie = async (query, method, page = 1) => {
  $('#result')
    .empty()
    .append(`<img src="img/loading-opaque.gif" class="img-fluid" alt="loading...">`);
  let movies;
  switch (method) {
    case 'name':
      movies = await findMoviesByName(query, page);
      break;
    case 'people':
      movies = await findMoviesByPeople(query, page);
      break;
  }
  fillMovies(movies);
  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="tooltip"]').on('click', function() {
    $(this).tooltip('hide');
  });
  createPagination(query, method, movies);
};

const getPersonDetail = async id => {
  const person = await getPerson(id);
  fillPersonDetail(person);
};

const getMovieDetail = async id => {
  const movie = await getMovie(id);
  fillMovieDetail(movie);
};

$(() => {
  nameBtnUI.click(() => {
    currentMethod = 'name';
  });
  peopleBtnUI.click(() => {
    currentMethod = 'people';
  });
  searchBtnUI.click(e => {
    const query = $('#query').val();
    searchMovie(query, currentMethod, 1);
  });
  $('#query').keydown(e => {
    if (event.keyCode === 13) {
      e.preventDefault();
      searchBtnUI.click();
    }
  });
});
